package ru.ilesho.dummyrx3.repository;

import java.util.ArrayList;
import io.reactivex.rxjava3.core.Observable;
import ru.ilesho.dummyrx3.model.Dummy;

public interface RepositoryInterface {
    Observable<ArrayList<Dummy>> getLabels();
}
