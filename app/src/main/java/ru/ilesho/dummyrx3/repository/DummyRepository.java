package ru.ilesho.dummyrx3.repository;

import java.util.ArrayList;

import io.reactivex.rxjava3.core.Observable;
import ru.ilesho.dummyrx3.model.Dummy;

public class DummyRepository implements RepositoryInterface {

    @Override
    public Observable<ArrayList<Dummy>> getLabels() {
        return Observable.create(emitter -> {
            try {

                ArrayList<Dummy> result = new ArrayList<>();
                for(int i=0; i < 5; i++) {
                    Thread.sleep( 1 * 1000);
                    Dummy item = Dummy.getInstance("some str " + i);
                    result.add(item);
                }

                emitter.onNext(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
                emitter.onError(e);
            } finally {
                emitter.onComplete();
            }
        });
    }
}
