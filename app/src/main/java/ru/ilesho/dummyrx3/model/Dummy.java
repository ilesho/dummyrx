package ru.ilesho.dummyrx3.model;

public class Dummy {

    private String label;

    public static Dummy getInstance(String label){
        Dummy result = new Dummy();
        result.setLabel(label);
        return result;
    }

    public String getLabel() {
        return label ;
    }

    public void setLabel(String label) {
        this.label = label ;
    }
}
