package ru.ilesho.dummyrx3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ru.ilesho.dummyrx3.model.Dummy;
import ru.ilesho.dummyrx3.repository.DummyRepository;
import ru.ilesho.dummyrx3.repository.RepositoryInterface;
import ru.ilesho.dummyrx3.view.DummyListAdapter;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private ListView listView;
    private ArrayAdapter<Dummy> adapter;
    RepositoryInterface repo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();
        init();
        getDataStart();

    }

    private void initView(){
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        progressBar = new ProgressBar(this);
        progressBar.setVisibility(View.GONE);
        layout.addView(progressBar);

        listView = new ListView(this);
        layout.addView(listView);

        setContentView(layout);
    }

    private void init() {
        adapter = new DummyListAdapter(this);
        listView.setAdapter(adapter);
        repo = new DummyRepository();
    }

    private void progressAnimationStart(){
        progressBar.setVisibility(View.VISIBLE);
    }


    private void progressAnimationStop(){
        progressBar.setVisibility(View.GONE);
    }


    private void getDataStart() {
        progressAnimationStart();
        repo.getLabels()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<Dummy>>() {

                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(ArrayList<Dummy> data) {
                        getDataResult(data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        progressAnimationStop();
                    }
                });
    }

    private void getDataResult(ArrayList<Dummy> data){
        updateData(data);
    }


    private void updateData(ArrayList<Dummy> newData){
        adapter.clear();
        adapter.addAll(newData);
    }

}
