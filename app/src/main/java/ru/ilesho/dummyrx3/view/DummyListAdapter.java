package ru.ilesho.dummyrx3.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import ru.ilesho.dummyrx3.model.Dummy;

public class DummyListAdapter extends ArrayAdapter<Dummy> {

    public DummyListAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1 );
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Dummy item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(android.R.layout.simple_list_item_1, null);
        }
        if (item != null) {
            ((TextView) convertView.findViewById(android.R.id.text1))
                    .setText(item.getLabel());
        }
        return convertView;
    }
}