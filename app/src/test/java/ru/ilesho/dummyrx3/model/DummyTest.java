package ru.ilesho.dummyrx3.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class DummyTest {

    @Test
    public void getInstance() {
        String testStr = "dummyLabel";

        Dummy dummy = Dummy.getInstance(testStr);

        assertNotNull(dummy);
        assertEquals(testStr, dummy.getLabel());

    }
}