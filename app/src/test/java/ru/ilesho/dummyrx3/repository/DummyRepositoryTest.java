package ru.ilesho.dummyrx3.repository;

import org.junit.Test;

import java.util.ArrayList;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ru.ilesho.dummyrx3.model.Dummy;

import static org.junit.Assert.*;

public class DummyRepositoryTest {


    ArrayList<Dummy> data  ;
    @Test
    public void getLabels() {

        RepositoryInterface repo = new DummyRepository();
        repo.getLabels()
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ArrayList<Dummy>>() {

                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(ArrayList<Dummy> newData) {
                        data = newData;
                    }

                    @Override
                    public void onError(Throwable ignored) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        try {
            Thread.sleep(7 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertNotNull(data);

    }
}